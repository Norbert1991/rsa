//
//  MessageTableViewCell.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "MessageTableViewCell.h"

@interface MessageTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *mainTitleLabel;

@end

@implementation MessageTableViewCell


- (void) setMainTitleValue: (NSString *) value {
    self.mainTitleLabel.text = value;
}


@end
