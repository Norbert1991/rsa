//
//  UncodedMessageTableViewCell.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "UncodedMessageTableViewCell.h"

@interface UncodedMessageTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *uncodedTitleLabel;

@end

@implementation UncodedMessageTableViewCell

- (void) setMainTitleValue: (NSString *) value withUncodedValue: (NSString *) uncodedValue {
    [super setMainTitleValue:value];
    
    self.uncodedTitleLabel.text = uncodedValue;
}


@end
