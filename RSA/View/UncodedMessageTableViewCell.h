//
//  UncodedMessageTableViewCell.h
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "MessageTableViewCell.h"

@interface UncodedMessageTableViewCell : MessageTableViewCell

- (void) setMainTitleValue: (NSString *) value withUncodedValue: (NSString *) uncodedValue;

@end
