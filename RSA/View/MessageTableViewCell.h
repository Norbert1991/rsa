//
//  MessageTableViewCell.h
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageTableViewCell : UITableViewCell

- (void) setMainTitleValue: (NSString *) value;

@end
