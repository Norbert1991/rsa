//
//  MessageViewController.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "MessageViewController.h"
#import "UncodedMessageTableViewCell.h"
#import "RSAKeyGenerator.h"
#import "Message.h"
#import "NSArray+RSA.h"
#import "NSString+RSA.h"

@interface MessageViewController ()<UITableViewDataSource>

@property (nonatomic, strong) RSAKeyGenerator *generator;
@property (nonatomic) struct RSAKey otherKey;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UITextField *inputTextField;

@property (nonatomic, retain) NSMutableArray *messages;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.generator = RSA_KEY_GEN;
    [self sendPublicKey];
    
    self.messages = [NSMutableArray array];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 120.0f;
    
    [self registerCommentObserver];
    [self registerKeyObserver];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) sendPublicKey {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"key" object:@{@"id": self.identifier, @"x": @([self.generator publicKey].x), @"y": @([self.generator publicKey].y)}];
    });
}

- (void) registerCommentObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNewComment:)
                                                 name:@"newMessage"
                                               object:nil];
}

- (void) registerKeyObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveKey:)
                                                 name:@"key"
                                               object:nil];
}

- (void) receiveNewComment: (NSNotification *) notification {
    if([notification.name isEqualToString:@"newMessage"]) {
        Message *message = notification.object;
        
        if(![message.identifer isEqualToString:self.identifier]) {
            [self.messages addObject:message];
            [self.tableView reloadData];
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.messages.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
}

- (void) receiveKey: (NSNotification *) notification {
    if([notification.name isEqualToString:@"key"]) {
        NSDictionary *message = notification.object;
        
        if(![message[@"id"] isEqualToString:self.identifier]) {
            struct RSAKey key;
            key.x = [message[@"x"] longValue];
            key.y = [message[@"y"] longValue];
            
            self.otherKey = key;
        }
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Message *message = self.messages[indexPath.row];
    
    if([message.identifer isEqualToString:self.identifier]) {
    
        MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"i"];
        [cell setMainTitleValue:message.message];
        return cell;
    }
    
    UncodedMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"y"];
    
    NSArray *coded = [message.message componentsSeparatedByString:@" "];
    
    [cell setMainTitleValue:message.message withUncodedValue:[coded uncodeRSA:[self.generator privateKey]]];
    return cell;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

- (IBAction)tapSend:(id)sender {
    Message *myMessage = [[Message alloc] init];
    myMessage.identifer = self.identifier;
    myMessage.message = self.inputTextField.text;
    
    [self.messages addObject:myMessage];
    self.inputTextField.text = nil;
    
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.messages.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    Message *codedMessage = [[Message alloc] init];
    codedMessage.identifer = self.identifier;
    codedMessage.message = [[myMessage.message codeRSA:self.otherKey] componentsJoinedByString:@" "];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"newMessage" object:codedMessage];
    });
    
}

@end
