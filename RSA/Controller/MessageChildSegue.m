//
//  MessageChildSegue.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "MessageChildSegue.h"
#import "MessageViewController.h"
#import "Masonry.h"

@implementation MessageChildSegue

- (void) perform {
    
    UIView *sourceView = self.sourceViewController.view;
    
    [self.sourceViewController addChildViewController:self.destinationViewController];
    [sourceView addSubview:self.destinationViewController.view];
    
    MessageViewController *controller = (MessageViewController *) self.destinationViewController;
    controller.identifier = self.identifier;
    
    [self.destinationViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.sourceViewController.view.mas_top);
        make.bottom.equalTo(self.sourceViewController.view.mas_bottom);
        make.width.equalTo(self.sourceViewController.view.mas_width).multipliedBy(0.5f);
        
        if([self.identifier isEqualToString:@"first"]) {
            make.left.equalTo(self.sourceViewController.view.mas_left);
        } else {
            make.right.equalTo(self.sourceViewController.view.mas_right);
        }
        
    }];
    
    [self.destinationViewController didMoveToParentViewController:self.sourceViewController];
}

@end
