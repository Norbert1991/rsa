//
//  RSAKeyGenerator.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "RSAKeyGenerator.h"

@interface RSAKeyGenerator()
    @property (nonatomic) struct RSAKey mpublicKey;
    @property (nonatomic) struct RSAKey mprivateKey;
@end

@implementation RSAKeyGenerator

- (id) init {
    self = [super init];
    
    if(self) {
        [self generatePQ:^(struct RSAKey public, struct RSAKey private) {
            self.mpublicKey = public;
            self.mprivateKey = private;
        }];
    }
    
    return self;
}

- (void) generatePQ:(void(^)(struct RSAKey public, struct RSAKey private)) callback {
    
    struct RSAKey publicKey;
    struct RSAKey privateKey;
    
    long p = -1;
    long q = -1;
    
    while(p == q) {
        p = [self randomBigFirstLong];
        q = [self randomBigFirstLong];
    }
    
    long phi = (p - 1) * (q - 1);
    long n   = p * q;
    
    long e, d;
    
    for (e = 3; [self nwdA:e b:phi] != 1; e+= 2);
    d = [self backModA:e n:phi];
    
    publicKey.x = e;
    publicKey.y = n;
    
    privateKey.x = d;
    privateKey.y = n;
    
    callback(publicKey, privateKey);
    
}

- (BOOL) isFirst: (long) value {
    
    if(value < 2) {
        return NO;
    }
    
    for (long i = 2; i * i <= value; i++) {
        if(value % i == 0) {
            return false;
        }
    }
    
    return true;
}

- (long) randomBigLongFrom: (long) from to: (long) to {
    return from + arc4random() % (to - from +1);
}

- (long) randomBigFirstLong {
    while (true) {
        long n = [self randomBigLongFrom:1000 to:10000];
        
        if([self isFirst:n]) {
            return n;
        }
    }
}

- (long) nwdA: (long) a b:(long) b {
    long t;
    
    while (b != 0) {
        t = b;
        b = a % b;
        a = t;
    }
    
    return a;
}

- (long) backModA: (long) a n: (long) n {
    
    long a0, n0, p0, p1, q, r, t;
    
    p0 = 0; p1 = 1; a0 = a; n0 = n;
    q  = n0 / a0;
    r  = n0 % a0;
    while(r > 0)
    {
        t = p0 - q * p1;
        if(t >= 0)
            t = t % n;
        else
            t = n - ((-t) % n);
        p0 = p1; p1 = t;
        n0 = a0; a0 = r;
        q  = n0 / a0;
        r  = n0 % a0;
    }
    return p1;
}

- (struct RSAKey) publicKey {
    return self.mpublicKey;
}

- (struct RSAKey) privateKey {
    return self.mprivateKey;
}


@end
