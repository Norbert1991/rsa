//
//  RSAKeyGenerator.h
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSAKey.h"

#define RSA_KEY_GEN [[RSAKeyGenerator alloc] init]

@interface RSAKeyGenerator : NSObject

- (struct RSAKey) publicKey;

- (struct RSAKey) privateKey;

@end
