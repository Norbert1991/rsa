//
//  NSString+RSA.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "NSString+RSA.h"
#import "RSAHelper.h"

@implementation NSString (RSA)

- (NSArray *) codeRSA: (struct RSAKey) key {
    long e = key.x;
    long n = key.y;
    
    NSMutableArray *codesArray = [NSMutableArray array];
    
    for(int i =0 ;i<[self length]; i++) {
        unichar character = [self characterAtIndex:i];
        long charInt = (long) character;
        
        long coded = [RSAHelper pot_modA:charInt w:e n:n];
        
        [codesArray addObject:@(coded)];
    }
    
    return codesArray;
}


@end
