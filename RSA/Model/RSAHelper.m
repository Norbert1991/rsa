//
//  RSAHelper.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "RSAHelper.h"

@implementation RSAHelper

+ (long) pot_modA:(long) a w: (long) w n: (long) n {
    
    long pot, wyn, q;
    
    pot = a; wyn = 1;
    for(q = w; q > 0; q /= 2)
    {
        if(q % 2) wyn = (wyn * pot) % n;
        pot = (pot * pot) % n;
    }
    return wyn;
}

@end
