//
//  Message.h
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic, retain) NSString *identifer;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSArray *codedMessage;

@end
