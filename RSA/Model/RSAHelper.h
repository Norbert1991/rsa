//
//  RSAHelper.h
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSAHelper : NSObject

+ (long) pot_modA:(long) a w: (long) w n: (long) n;

@end
