//
//  NSString+RSA.h
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSAKey.h"

@interface NSString (RSA)

- (NSArray *) codeRSA: (struct RSAKey) key;

@end
