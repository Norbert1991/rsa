//
//  NSArray+RSA.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "NSArray+RSA.h"
#import "RSAHelper.h"

@implementation NSArray (RSA)

- (NSString *) uncodeRSA: (struct RSAKey) key {
    long e = key.x;
    long n = key.y;
    
    NSString *message = @"";
    
    for (int i = 0; i < [self count] ; i++) {
        
        NSString *stringValue = (NSString *) self[i];
        
        long charLong = [stringValue longLongValue];
        
        long uncoded = [RSAHelper pot_modA:charLong w:e n:n];
        unichar uncodedChar = (unichar) uncoded;
        
        message = [NSString stringWithFormat:@"%@%C", message, uncodedChar];
    }
    
    return [NSString stringWithUTF8String:[message cStringUsingEncoding:NSUTF8StringEncoding]];
}

@end
