//
//  ViewController.m
//  RSA
//
//  Created by Norbert Szydłowski on 28/12/2016.
//  Copyright © 2016 Norbert Szydłowski. All rights reserved.
//

#import "ViewController.h"
#import "RSAKeyGenerator.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self performSegueWithIdentifier:@"first" sender:self];
    [self performSegueWithIdentifier:@"second" sender:self];
    
}




@end
